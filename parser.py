import sys
import re
import xml.etree.ElementTree as ET
from pathlib import Path
from urllib.parse import urljoin
from xml.dom import minidom

import requests
from bs4 import BeautifulSoup as BS
from tidylib import tidy_document

div_list = object()


class CIAParser:

    PAGES = {
        "madagascar": {
            "url": "https://www.cia.gov/library/publications/the-world-factbook/geos/ma.html",
            # "file": "data/madagascar.html",
            "exclude": {},
        },
        "bangladesh": {
            "url": "https://www.cia.gov/library/publications/the-world-factbook/geos/bg.html",
            # "file": "data/bangladesh.html",
            "exclude": {},
        },
        "jersey": {
            "url": "https://www.cia.gov/library/publications/the-world-factbook/geos/je.html",
            # "file": "data/jersey.html",
            "exclude": {"transnational"},
        },
        "iran": {
            "url": "https://www.cia.gov/library/publications/the-world-factbook/geos/ir.html",
            # "file": "data/iran.html",
            "exclude": {},
        }
    }

    def process_text(self, text):
        text = re.sub(r"\([1-9 ]*[a-zA-Z ]*[0-9]{4}( est\.)*( es)*\)", "", text)
        text = re.sub(r"[\s]+", " ", text).strip()

        return text.strip()

    @classmethod
    def generate_documents(cls):
        try:
            verbosity = sys.argv.index("--verbosity") >= 0
        except ValueError:
            verbosity = False

        for page, config in cls.PAGES.items():
            parser = CIAParser(page, config["url"], config.get("file"), verbosity=verbosity)

            root = ET.Element("country", **{"generated-from": parser.url, "id": page})

            attributes = ["parse_intro", "parse_pictures"] + \
                         sorted(list(set(dir(CIAParser)) - {"parse_intro", "parse_pictures"}))

            for attr in attributes:
                if not attr.startswith("parse_"):
                    continue
                if attr.replace("parse_", "") in config["exclude"]:
                    continue

                getattr(parser, attr)(root)

            rough_string = ET.tostring(root, encoding="unicode")
            reparsed = minidom.parseString(rough_string)
            Path(f"_build/{page}.xml").write_bytes(reparsed.toprettyxml(indent="  ", encoding="utf-8"))

    def process_response(self, response):
        document, errors = tidy_document(response)

        self.soup = BS(document, "html.parser")

        for x in self.soup.findAll(["link", "script"]):
            x.decompose()

        for tag in self.soup.findAll():
            for key in ["name", "style"]:
                try:
                    tag.attrs.pop(key)
                except KeyError:
                    pass

    def __init__(self, key, url, file, verbosity):
        self.key = key
        self.url = url
        self.file = file
        self.soup = None
        self.verbosity = verbosity

        if self.file is None:
            response = requests.get(self.url).content
        else:
            response = Path(self.file).read_text()

        self.process_response(response)

        (Path("data") / f"{key}.html").write_text(str(self.soup.prettify()))

    def absolute_url(self, path):
        return urljoin(self.url, path)

    def parse_pictures(self, root):
        pictures = ET.SubElement(root, "pictures")
        flag = ET.SubElement(pictures, "picture", description="Flag")
        flag.text = self.absolute_url(self.soup.find("div", {"class": "flagBox"}).findChild("img")["src"])

        locator = ET.SubElement(pictures, "picture", description="Location")
        locator.text = self.absolute_url(self.soup.find("div", {"class": "locatorBox"}).findChild("img")["src"])

        map_ = ET.SubElement(pictures, "picture", description="Map")
        map_.text = self.absolute_url(self.soup.find("div", {"class": "mapBox"}).findChild("img")["src"])

        photos = ET.SubElement(pictures, "photos")

        modal = self.soup.find("div", {"id": "wfbPhotoGalleryModal"})

        for item in modal.findChildren("div", {"class": "item"}):
            photo = ET.SubElement(photos, "photo",
                                  description=self.process_text(item.find("div", {"class": "photoCaption"}).get_text()))
            photo.text = self.absolute_url(item.find("img")["src"])

    def parse_intro(self, root):
        intro = ET.SubElement(root, "introduction")

        header = self.soup.find("h2", {"sectiontitle": "Introduction"})

        ET.SubElement(intro, "name").text = self.process_text(header.find("span", {"class": "region"}).get_text())

        background = ET.SubElement(intro, "background")

        li = header.parent.find_next("li")

        for paragraph in li.findAll("div", {"class": "category_data"}):
            ET.SubElement(background, "paragraph").text = self.process_text(paragraph.get_text())

    def get_section(self, section):
        header = self.soup.find("h2", {"sectiontitle": section})
        return header.parent.find_next("li")

    def get_simple_data(self, li, name):
        for a in li.findAll("a"):
            if a.get_text().strip() != name:
                continue

            return a.parent.find_next("div", {"class": "category_data"})

        return None
        # return li.find("a", text=name).parent.find_next("div", {"class": "category_data"})

    def simple_list(self, li, element, elements):
        for simple_prop in elements:
            convert = None
            if isinstance(simple_prop, tuple):
                name = simple_prop[0]
                value = simple_prop[1]

                if len(simple_prop) >= 3:
                    convert = simple_prop[2]
            else:
                name = simple_prop
                value = f"{simple_prop}:"

            try:
                if convert is None:
                    text = self.get_simple_data(li, value).get_text()
                    ET.SubElement(element, "detail", name=name).text = self.process_text(text)
                else:
                    element_name = "detail"
                    if convert == list or convert is div_list:
                        element_name = "list-detail"

                    el = ET.SubElement(element, element_name, name=name)

                    if convert == list:
                        try:
                            text = self.get_simple_data(li, value).get_text()
                        except AttributeError:
                            element.remove(el)
                            raise

                        if len(text.split(";")) == 1:
                            sep = ","
                        else:
                            sep = ";"

                        i = 0
                        for x in text.split(sep):
                            ET.SubElement(el, "list-item").text = self.process_text(x.strip())
                            i += 1

                        if i == 0:
                            element.remove(el)

                    elif convert is div_list:
                        a_container = None

                        for a in li.findAll("a"):
                            if a.get_text().strip() == value:
                                a_container = a.parent
                                break

                        if a_container is not None:
                            i = 0
                            for l in a_container.find_all_next("div"):
                                if "category_data" not in l.get("class", ""):
                                    break

                                ET.SubElement(el, "list-item").text = self.process_text(l.get_text().strip())
                                i += 1

                            if i == 0:
                                element.remove(el)
                        else:
                            element.remove(el)

            except AttributeError as e:
                if self.verbosity:
                    print(self.key, name, e)
                pass  # not present in the CIA fact book

    def structured_list(self, li, element, elements):
        for prop in elements:
            if isinstance(prop, str):
                prop = {"name": prop}

            try:
                div_field = li.find("a", text=prop.get("orig", f"{prop['name']}:")).parent
            except AttributeError:
                continue

            single_element = prop.get("single_element", False)

            element_name = "detail"
            if not single_element:
                element_name = "nested-detail"

            subelement = ET.SubElement(element, element_name, name=prop["name"])

            def fl(x):
                if isinstance(x, tuple):
                    return x[0]
                return x

            allowed_attrs = None
            if "attrs" in prop:
                allowed_attrs = [fl(x) for x in prop["attrs"]]

            convert = {}
            for x in prop.get("attrs", []):
                if isinstance(x, tuple):
                    convert[x[0]] = x[1]

            i = 0
            for el in div_field.find_all_next("div"):
                if el.get("id") == "field":
                    break

                try:
                    category = el.find("span", {"class": "category"}).get_text().replace(":", "").strip()
                except AttributeError:
                    continue
                local_convert = None
                if allowed_attrs is not None:
                    matched = False
                    for x in allowed_attrs:
                        if not isinstance(x, str) and re.match(x, category) is not None:
                            if x in convert:
                                local_convert = convert[x]
                            matched = True
                            break
                        elif x == category:
                            if x in convert:
                                local_convert = convert[x]
                            matched = True
                            break
                    if not matched:
                        continue

                text = self.process_text(el.find("span", {"class": "category_data"}).get_text().strip())

                if single_element:
                    subelement.text = text
                else:
                    if local_convert == list:
                        subsub = ET.SubElement(subelement, "list-detail", name=category)
                        text = re.sub("([1-9]+),([1-9]+)", r"\1\2", text)
                        for x in text.split(","):
                            ET.SubElement(subsub, "list-item").text = x.strip()
                    else:
                        subsub = ET.SubElement(subelement, "detail", name=category)
                        subsub.text = text
                i += 1
            if i == 0:
                element.remove(subelement)

    def parse_geography(self, root):
        geography = ET.SubElement(root, "section", name="Geography")
        li = self.get_section("Geography")

        self.simple_list(li, geography, [
            "Location",
            ("Coordinates", "Geographic coordinates:"),
            "Coastline",
            "Climate",
            "Terrain",
            ("Natural resources", "Natural resources:", list),
            "Irrigated land",
            ("Natural hazards", "Natural hazards:", list),
            ("Current environment issues", "Environment - current issues:", list),
            "Population - distribution",
        ])

        self.structured_list(li, geography, [
            {"name": "Area", "attrs": ["total", "land", "water"]},
            "Maritime claims",
            {"name": "Land use", },
            {"name": "Environment international agreements", "orig": "Environment - international agreements:"}
        ])

        if self.key != "jersey":
            self.structured_list(li, geography, [
                {"name": "Elevation", },  # TODO: extremes
            ])

        if self.key in {"madagascar", "jersey"}:
            self.simple_list(li, geography, ["Land boundaries"])
        else:
            self.structured_list(li, geography, [
                {"name": "Land boundaries", "attrs": ["total", (re.compile("border countries \([1-9]+\)"), list)]},
            ])

    def age_structure(self, li, element):
        try:
            div_field = li.find("a", text="Age structure:").parent
        except AttributeError:
            return
        subelement = ET.SubElement(element, "histogram", name="Age structure", unit="%")

        for el in div_field.find_all_next("div"):
            if el.get("id") == "field":
                break

            try:
                category = el.find("span", {"class": "category"}).get_text().replace(":", "").strip()
            except AttributeError:
                continue

            try:
                from_age, to_age = {
                    "0-14 years": ("0", "14"),
                    "15-24 years": ("15", "24"),
                    "25-54 years": ("25", "54"),
                    "55-64 years": ("55", "64"),
                    "65 years and over": ("65", None)
                }[category]
            except KeyError:
                continue

            text = self.process_text(el.find("span", {"class": "category_data"}).get_text().strip())

            search = re.search("([0-9.]+)% \(male ([0-9,]+)/female ([0-9,]+)\)", text)
            if search:
                if to_age:
                    subsub = ET.SubElement(subelement, "entry", **{"from-age": from_age, "to-age": to_age},
                                           # male=search.group(2).replace(",", ""),
                                           # female=search.group(3).replace(",", "")
                                           )
                else:
                    subsub = ET.SubElement(subelement, "entry", **{"from-age": from_age},
                                           # male=search.group(2).replace(",", ""),
                                           # female=search.group(3).replace(",", "")
                                           )
                subsub.text = search.group(1)

    def sex_ratio(self, li, element):
        try:
            div_field = li.find("a", text="Sex ratio:").parent
        except AttributeError as e:
            if self.verbosity:
                print(e)
            return

        subelement = ET.SubElement(element, "histogram", name="Sex ratio", description="male to female")

        for el in div_field.find_all_next("div"):
            if el.get("id") == "field":
                break

            try:
                category = el.find("span", {"class": "category"}).get_text().replace(":", "").strip()
            except AttributeError:
                continue

            try:
                from_age, to_age = {
                    "at birth": ("0", "0"),
                    "0-14 years": ("0", "14"),
                    "15-24 years": ("15", "24"),
                    "25-54 years": ("25", "54"),
                    "55-64 years": ("55", "64"),
                    "65 years and over": ("65", None),
                    "total population": (None, None)
                }[category]
            except KeyError:
                continue

            text = self.process_text(el.find("span", {"class": "category_data"}).get_text().strip())

            search = re.search("([0-9.]+) ", text)
            if search:
                if to_age and from_age:
                    subsub = ET.SubElement(subelement, "entry", **{"from-age": from_age, "to-age": to_age})
                elif from_age:
                    subsub = ET.SubElement(subelement, "entry", **{"from-age": from_age})
                else:
                    subsub = ET.SubElement(subelement, "entry")
                subsub.text = search.group(1)

    def parse_people_and_society(self, root):
        people_and_society = ET.SubElement(root, "section", name="People and society")
        li = self.get_section("People and Society")

        self.simple_list(li, people_and_society, [
            "Population",
            ("Ethnic groups", "Ethnic groups:", list),
            ("Languages", "Languages:", list),  # TODO: Bangladesh commas
            "Religions",
            ("Demographic profile", "Demographic profile:", div_list),
            "Population growth rate",
            "Birth rate",
            "Death rate",
            "Net migration rate",
            "Population distribution",
            "Mother's mean age at first birth",
            "Maternal mortality rate",
            "Total fertility rate",
            "Contraceptive prevalence rate",
            "Health expenditures",
            "Physicians density",
            "Hospital bed density",
            "Total fertility rate",
            ("Urban areas", "Major urban areas - population:", list),
            "HIV/AIDS - adult prevalence rate",
            "HIV/AIDS - people living with HIV/AIDS",
            "HIV/AIDS - deaths",
            "Obesity - adult prevalence rate",
            "Children under the age of 5 years underweight",
            "Education expenditures",

        ])

        self.structured_list(li, people_and_society, [
            {"name": "Nationality", "attrs": ["noun"], "single_element": True},
            {"name": "Dependency ratios", "attrs": ["total dependency ratio",
                                                    "youth dependency ratio",
                                                    "elderly dependency ratio"]},
            {"name": "Median age", "attrs": ["total", "male", "female"]},
            "Urbanization",
            {"name": "Infant mortality rate", "attrs": ["total",
                                                        "male",
                                                        "female"]},
            {"name": "Life expectancy at birth", "attrs": ["total population",
                                                           "male",
                                                           "female"]},
            "Major infectious diseases",
            "Literacy",
            "School life expectancy (primary to tertiary education)",
            {"name": "Unemployment, youth ages 15-24", "attrs": ["total", "male", "female"]}
        ])

        self.age_structure(li, people_and_society)
        self.sex_ratio(li, people_and_society)

    def parse_government(self, root):
        government = ET.SubElement(root, "section", name="Government")
        li = self.get_section("Government")

        self.simple_list(li, government, [
            "Dependency status",
            "Government type",
            ("Administrative divisions", "Administrative divisions:", list),
            ("Independence", "Independence:", list),
            "National holiday",
            "Legal system",
            "International law organization participation",
            "Suffrage",
            ("International organization participation", "International organization participation:", list),
            "Flag description",
            ("National symbol(s)", "National symbol(s):", list),
        ])

        self.structured_list(li, government, [
            "Country name",
            "Capital",
            "Constitution",
            "Citizenship",
            "Executive branch",
            "Legislative branch",
            "Judicial branch",
            "National anthem"
        ])

        if self.key != "jersey":
            self.simple_list(li, government, [
                ("Political parties and leaders", "Political parties and leaders:", div_list),
            ])
        else:
            self.structured_list(li, government, [
                "Political parties and leaders"
            ])

        if self.key in {"iran", "jersey"}:
            self.simple_list(li, government, [
                "Diplomatic representation in the US",
                "Diplomatic representation from the US",
            ])

        if self.key in {"madagascar", "bangladesh"}:
            self.simple_list(li, government, [
                ("Political pressure groups and leaders", "Political pressure groups and leaders:", div_list)
            ])

            self.structured_list(li, government, [
                "Diplomatic representation in the US",
                "Diplomatic representation from the US",
            ])

    def parse_economy(self, root):
        economy = ET.SubElement(root, "section", name="Economy")
        li = self.get_section("Economy")

        self.simple_list(li, economy, [
            ("Overview", "Economy - overview:"),
            "GDP (purchasing power parity)",
            "GDP (official exchange rate)",
            ("GDP per capita", "GDP - per capita (PPP):"),
            "Gross national saving",
            ("Agricultural products", "Agriculture - products:", list),
            "Industries",
            "Industrial production growth rate",
            "Labor force",
            "Unemployment rate",
            "Population below poverty line",
            "Distribution of family income - Gini index",
            "Taxes and other revenues",
            "Budget surplus (+) or deficit (-)",
            "Public debt",
            "Fiscal year",
            "Inflation rate (consumer prices)",
            "Central bank discount rate",
            "Commercial bank prime lending rate",
            "Stock of narrow money",
            "Stock of broad money",
            "Stock of domestic credit",
            "Market value of publicly traded shares",
            "Current account balance",
            "Exports",
            ("Export commodities", "Exports - commodities:"),
            ("Export partners", "Exports - partners:"),
            "Imports",
            ("Import commodities", "Imports - commodities:"),
            ("Import partners", "Imports - partners:"),
            "Reserves of foreign exchange and gold",
            "Debt - external",
            "Stock of direct foreign investment - at home",
            "Stock of direct foreign investment - abroad",
        ])

        self.structured_list(li, economy, [
            "GDP - composition, by end use",
            "GDP - composition, by sector of origin:",
            "Labor force - by occupation",
            "Household income or consumption by percentage share",
            "Budget",
        ])

    def parse_energy(self, root):
        energy = ET.SubElement(root, "section", name="Energy")
        li = self.get_section("Energy")

        self.simple_list(li, energy, [
            "Electricity - production",
            "Electricity - consumption",
            "Electricity - exports",
            "Electricity - imports",
            "Electricity - installed generating capacity",
            "Electricity - from fossil fuels",
            "Electricity - from nuclear fuels",
            "Electricity - from hydroelectric plants",
            "Electricity - from other renewable sources",
            "Crude oil - production",
            "Crude oil - exports",
            "Crude oil - imports",
            "Crude oil - proved reserves",
            "Refined petroleum products - production",
            "Refined petroleum products - consumption",
            "Refined petroleum products - exports",
            "Refined petroleum products - imports",
            "Natural gas - production",
            "Natural gas - consumption",
            "Natural gas - exports",
            "Natural gas - imports",
            "Natural gas - proved reserves",
            "Carbon dioxide emissions from consumption of energy",
        ])

        self.structured_list(li, energy, [
            "Electricity access",
        ])

    def parse_communications(self, root):
        communications = ET.SubElement(root, "section", name="Communications")
        li = self.get_section("Communications")

        self.simple_list(li, communications, [
            "Broadcast media",
            "Internet country code",

        ])

        self.structured_list(li, communications, [
            {"name": "Telephones - fixed lines", "attrs": [
                "total subscriptions",
                "subscriptions per 100 inhabitants"
            ]},
            {"name": "Telephones - mobile cellular", "attrs": [
                "total",
                "subscriptions per 100 inhabitants",
            ]},
            "Telephone system",
            {"name": "Internet users", "attrs": [
                "total",
                "percent of population"
            ]}
        ])

    def parse_transportation(self, root):
        transportation = ET.SubElement(root, "section", name="Transportation")
        li = self.get_section("Transportation")

        self.simple_list(li, transportation, [
            "Civil aircraft registration country code prefix",
            "Airports",
            "Heliports",
            ("Pipelines", "Pipelines:", list),
            "Waterways",
        ])

        self.structured_list(li, transportation, [
            "National air transport system",
            "Airports - with paved runways",
            "Airports - with unpaved runways",
            {"name": "Railways", "attrs": [
                "total",
                "broad gauge",
                "standard gauge"
            ]},
            {"name": "Roadways", "attrs": [
                "total",
                "paved",
                "unpaved"
            ]},
            {"name": "Merchant marine", "attrs": [
                "total",
                "by type",
                "foreign-owned",
                "registered in other countries"
            ]},
            "Ports and terminals"
        ])

    def parse_military_and_security(self, root):
        military_and_security = ET.SubElement(root, "section", name="Military and security")
        li = self.get_section("Military and Security")

        self.simple_list(li, military_and_security, [
            "Military expenditures",
            ("Military service age and obligation", "Military service age and obligation:", list),
            "Maritime threats",
            "Military - note"
        ])

        if self.key in {"madagascar", "bangladesh"}:
            self.simple_list(li, military_and_security, [
                "Military branches",
            ])
        else:
            self.simple_list(li, military_and_security, [
                ("Military branches", "Military branches:", list),
            ])

    def parse_transnational(self, root):
        transnational = ET.SubElement(root, "section", name="Transnational issues")
        li = self.get_section("Transnational Issues")

        self.simple_list(li, transnational, [
            ("Disputes - international", "Disputes - international:", list),
            ("Illicit drugs", "Illicit drugs:", list)
        ])

        self.structured_list(li, transnational, [
            "Refugees and internally displaced persons",
            "Trafficking in persons:",
        ])


if __name__ == "__main__":
    CIAParser.generate_documents()
