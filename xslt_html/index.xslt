<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/countries">
        <html>
            <head>
                <title>Countries</title>

                <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
                <link rel="stylesheet"
                      href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
                <link rel="stylesheet" href="style.css"/>
                <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"/>
                <script type="text/javascript"
                        src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"/>

            </head>
            <body>
                <nav class="white" role="navigation">
                    <div class="nav-wrapper container">
                        <a id="logo-container" href="index.html" class="brand-logo">Countries</a>
                    </div>
                </nav>

                <div class="container">
                    <div class="section">
                        <div class="collection">
                            <xsl:apply-templates select="country" />
                        </div>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="country">
        <a class="collection-item">
            <xsl:attribute name="href">
                <xsl:value-of select="@id" />
                <xsl:text>.html</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="introduction/name"/>
        </a>
    </xsl:template>
</xsl:stylesheet>
