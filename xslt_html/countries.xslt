<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/countries">
        <xsl:for-each select="country">
            <xsl:result-document method="html" href="_build/html/{@id}.html" indent="yes">
                <xsl:apply-templates select="."/>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="country">
        <html>
            <head>
                <title>
                    <xsl:value-of select="introduction/name"/>
                </title>

                <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
                <link rel="stylesheet"
                      href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"/>
                <link rel="stylesheet" href="style.css"/>
                <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"/>
                <script type="text/javascript"
                        src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"/>

            </head>
            <body>
                <nav class="white" role="navigation">
                    <div class="nav-wrapper container">
                        <a id="logo-container" href="index.html" class="brand-logo">Countries</a>
                    </div>
                </nav>

                <div class="container">
                    <div class="section">
                        <h1>
                            <xsl:value-of select="introduction/name"/>
                        </h1>

                        <xsl:apply-templates select="introduction/background"/>
                        <ul class="collapsible popout" data-collapsible="accordion">
                            <xsl:apply-templates select="pictures"/>
                            <xsl:apply-templates select="section"/>
                        </ul>
                    </div>
                </div>

            </body>
        </html>
    </xsl:template>

    <xsl:template match="introduction/background">
        <xsl:for-each select="paragraph">
            <p>
                <xsl:value-of select="."/>
            </p>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="pictures">
        <li>
            <div class="collapsible-header">
                <i class="material-icons">photo_library</i>
                Photos
            </div>
            <div class="collapsible-body">
                <div class="row">
                    <xsl:apply-templates select="picture"/>
                </div>
                <div class="row">
                    <xsl:apply-templates select="photos/photo"/>
                </div>
            </div>
        </li>

    </xsl:template>

    <xsl:template match="picture">
        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img class="responsive-img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                        <xsl:attribute name="alt">
                            <xsl:value-of select="@description"/>
                        </xsl:attribute>
                    </img>
                </div>
                <div class="card-content">
                    <span class="card-title grey-text text-darken-4">
                        <xsl:value-of select="@description"/>
                    </span>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="photo">
        <div class="col s12 m6 l3">
            <div class="card">
                <div class="card-image">
                    <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                        <xsl:attribute name="alt">
                            <xsl:value-of select="@description"/>
                        </xsl:attribute>
                    </img>
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Photo
                        <i class="material-icons right">more_vert</i>
                    </span>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Photo
                        <i class="material-icons right">close</i>
                    </span>
                    <p>
                        <xsl:value-of select="@description"/>
                    </p>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="section">
        <li>
            <div class="collapsible-header">
                <i class="material-icons">
                    <xsl:choose>
                        <xsl:when test="@name = 'Geography'">
                            public
                        </xsl:when>
                        <xsl:when test="@name = 'Economy'">
                            attach_money
                        </xsl:when>
                        <xsl:when test="@name = 'Energy'">
                            power
                        </xsl:when>
                        <xsl:when test="@name = 'Communications'">
                            local_phone
                        </xsl:when>
                        <xsl:when test="@name = 'Transnational issues'">
                            priority_high
                        </xsl:when>
                        <xsl:when test="@name = 'People and society'">
                            people
                        </xsl:when>
                        <xsl:when test="@name = 'Transportation'">
                            train
                        </xsl:when>
                        <xsl:when test="@name = 'Government'">
                            assignment
                        </xsl:when>
                        <xsl:when test="@name = 'Military and security'">
                            security
                        </xsl:when>
                        <xsl:otherwise>
                            wrap_text
                        </xsl:otherwise>
                    </xsl:choose>
                </i>
                <xsl:value-of select="@name"/>
            </div>
            <div class="collapsible-body">
                <ul class="collection">
                    <xsl:apply-templates/>
                </ul>
            </div>
        </li>
    </xsl:template>

    <xsl:template match="detail">
        <li class="collection-item">
            <strong>
                <xsl:value-of select="@name"/>
            </strong>
            :
            <xsl:value-of select="."/>
        </li>
    </xsl:template>

    <xsl:template match="list-detail">
        <li class="collection-item">
            <strong>
                <xsl:value-of select="@name"/>
            </strong>
            :
            <ul class="browser-default">
                <xsl:apply-templates select="list-item"/>
            </ul>
        </li>
    </xsl:template>

    <xsl:template match="list-item">
        <li>
            <xsl:value-of select="."/>
        </li>
    </xsl:template>

    <xsl:template match="nested-detail">
        <li class="collection-item">
            <strong>
                <xsl:value-of select="@name"/>
            </strong>
            :
            <ul class="collection">
                <xsl:apply-templates select="detail|list-detail"/>
            </ul>
        </li>
    </xsl:template>

    <xsl:template match="histogram">
        <li class="collection-item">
            <strong>
                <xsl:value-of select="@name"/>
            </strong>
            :
            <xsl:if test="not(empty(@description))">
                <small>
                    (<xsl:value-of select="@description"/>)
                </small>
            </xsl:if>
            <ul class="collection">
                <xsl:apply-templates select="entry"/>
            </ul>
        </li>
    </xsl:template>

    <xsl:template match="entry">
        <li class="collection-item">
            <div class="row without-margin">
                <div class="col s8">
                    <xsl:choose>
                        <xsl:when test="empty(@from-age) and empty(@to-age)">
                            <strong>Overall</strong>:
                        </xsl:when>
                        <xsl:when test="(@from-age = '0') and (@to-age = '0')">
                            <strong>At birth</strong>:
                        </xsl:when>
                        <xsl:when test="empty(@to-age)">
                            <strong>
                                Over
                                <xsl:value-of select="@from-age"/>
                            </strong>
                            :
                        </xsl:when>
                        <xsl:otherwise>
                            <strong>
                                From age
                                <xsl:value-of select="@from-age"/>
                                to age
                                <xsl:value-of select="@to-age"/>
                            </strong>
                            :
                        </xsl:otherwise>
                    </xsl:choose>
                </div>
                <div class="col s4">
                    <xsl:value-of select="."/>
                    <xsl:value-of select="../@unit"/>
                </div>
            </div>
        </li>
    </xsl:template>

</xsl:stylesheet>
