# Term project for BI-XML, 2017/2018

Graded A.
Everything except for the backups of HTMLs from CIA Factbook is licensed under MIT.
The information in the generated files from the CIA Factbook is in the public domain (see https://www.cia.gov/library/publications/the-world-factbook/docs/contributor_copyright.html)


How to run
----------

Python 3.6 is required, create a virtualenv and install requirements (from `requirements.txt`)

After that run

```
make all
```

Project structure:
------------------

`_build`:
  Genareted XML (single countries files and countries.xml)

`_build/html`:
  Generated HTML (and some CSS styles)

`data`:
  Backup of source HTML

`validation`:
  DTD and RNC validation

`xslt_html`:
  Two separated styles for HTML, one for single countries and one for index.html

`xslt_join`:
  ALternative way of joining single countries to one XML

`countries.dtd`:
  A DTD for joing countries to one XML

`Makefile`:
  The collection of all needed commands

`parser.py`:
  Python 3.6 parser of the CIA pages

`pdf.xslt`:
  The style for PDF

`requirements.txt`:
  Python 3.6 libraries for running parser.py

Assignment (in Czech)
---------------------

Příprava vstupního XML: pro každou zemi vlastní XML dokument. Dokumenty budou propojeny do jediného dokumentu s využitím DTD. Všechny části projektu budou generovány z tohoto dokumentu, ale každá část projektu může být generována s pomocí jiné XSLT (doporučuji), ale můžete i vše spojit do jediné XSLT.

  * Website: minimální počet stránek: 4 - index a stránka pro každou zemi, vyšší počet stránek kvůli lepší přehlednosti vítán, ale není podmínkou
  * 1 PDF dokument - generován s pomocí XSL-FO a Apache FOP (https://xmlgraphics.apache.org/fop/). Někdy zlobí čeština, proto doporučuji vytvářet minimálně PDF v angličtině.
  * 1 eBook ve formátu ePub, obdoba webové stránky
  * XML soubory jednotlivých zemí validovány s DTD a RelaxNG

