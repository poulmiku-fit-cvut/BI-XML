COUNTRIES = _build/bangladesh.xml _build/iran.xml _build/jersey.xml _build/madagascar.xml
COUNTRIES_HTML = _build/html/bangladesh.html _build/html/iran.html _build/html/jersey.html _build/html/madagascar.html

all: _build/countries.xml _build/html/index.html $(COUNTRIES_HTML) _build/countries.pdf poulmiku.zip

validate_rnc: $(COUNTRIES) validation/country.rnc
	jing -c validation/country.rnc $(COUNTRIES)

validate_dtd: $(COUNTRIES) validation/country.dtd
	xmllint --noout --dtdvalid validation/country.dtd $(COUNTRIES)

convert_rnc_to_dtd: validation/country.rnc
	trang -I rnc -O dtd validation/country.rnc validation/country2.dtd

_build/countries.xml: $(COUNTRIES) countries.dtd
	xmllint --dropdtd --noent countries.dtd > _build/countries.xml

xslt_join: $(COUNTRIES) xslt_join/base_countries.xml xslt_join/countries.xslt
	saxon xslt_join/base_countries.xml xslt_join/countries.xslt > _build/countries.xml

_build/html/index.html: _build/countries.xml xslt_html/index.xslt
	saxon _build/countries.xml xslt_html/index.xslt > _build/html/index.html

$(COUNTRIES_HTML): $(COUNTRIES) xslt_html/countries.xslt
	saxon _build/countries.xml xslt_html/countries.xslt

$(COUNTRIES): parser.py
	python parser.py

#_build/countries.pdf: _build/countries.xml pdf.xslt
#	fop -xml _build/countries.xml -xsl pdf.xslt -pdf _build/countries.pdf

_build/countries.fo: _build/countries.xml pdf.xslt
	saxon _build/countries.xml pdf.xslt > _build/countries.fo

_build/countries.pdf: _build/countries.fo pdf.xslt
	fop -fo _build/countries.fo -pdf _build/countries.pdf

poulmiku.zip: _build/countries.xml _build/html/index.html $(COUNTRIES_HTML) _build/countries.pdf README.md
	rm -rf poulmiku.zip
	zip -r poulmiku.zip _build data validation xslt_html xslt_join countries.dtd Makefile parser.py pdf.xslt README.md requirements.txt -x *.git*
