<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4-portrait" page-height="29.7cm" page-width="21.0cm" margin="2cm"
                                       margin-bottom="1cm">
                    <fo:region-body/>
                    <fo:region-after/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:declarations>
                <x:xmpmeta xmlns:x="adobe:ns:meta/">
                    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                        <rdf:Description rdf:about="" xmlns:dc="http://purl.org/dc/elements/1.1/">
                            <!-- Dublin Core properties go here -->
                            <dc:title>World Factbook</dc:title>
                        </rdf:Description>
                    </rdf:RDF>
                </x:xmpmeta>
            </fo:declarations>
            <fo:page-sequence master-reference="A4-portrait">
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="center">
                        Page
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-size="18pt" font-weight="500" text-align="center">
                        World Factbook
                    </fo:block>
                    <xsl:apply-templates/>
                </fo:flow>

            </fo:page-sequence>

        </fo:root>
    </xsl:template>

    <xsl:template match="countries">
        <fo:block>
            Available countries:
        </fo:block>
        <fo:list-block provisional-distance-between-starts="0.3cm" provisional-label-separation="0.15cm">
            <xsl:for-each select="country">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block>
                            <fo:inline>&#183;</fo:inline>
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <fo:basic-link internal-destination="{@id}" color="blue" text-decoration="underline">
                                <xsl:value-of select="introduction/name"/>
                            </fo:basic-link>

                            <fo:list-block provisional-distance-between-starts="0.3cm"
                                           provisional-label-separation="0.15cm"
                                           margin-left="15pt">

                                <fo:list-item>
                                    <fo:list-item-label end-indent="label-end()">
                                        <fo:block>
                                            <fo:inline>&#183;</fo:inline>
                                        </fo:block>
                                    </fo:list-item-label>
                                    <fo:list-item-body start-indent="body-start()">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{generate-id(pictures)}" color="blue"
                                                           text-decoration="underline">
                                                Pictures
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>

                                <xsl:for-each select="section">
                                    <fo:list-item>
                                        <fo:list-item-label end-indent="label-end()">
                                            <fo:block>
                                                <fo:inline>&#183;</fo:inline>
                                            </fo:block>
                                        </fo:list-item-label>
                                        <fo:list-item-body start-indent="body-start()">
                                            <fo:block>
                                                <fo:basic-link internal-destination="{generate-id(.)}" color="blue"
                                                               text-decoration="underline">
                                                    <xsl:value-of select="@name"/>
                                                </fo:basic-link>
                                            </fo:block>
                                        </fo:list-item-body>
                                    </fo:list-item>
                                </xsl:for-each>
                            </fo:list-block>

                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
        </fo:list-block>
        <xsl:apply-templates select="country"/>
    </xsl:template>

    <xsl:template match="country">
        <fo:block page-break-before="always"/>
        <fo:block font-size="15pt" font-weight="500" text-align="center">
            <xsl:attribute name="id">
                <xsl:value-of select="@id"/>
            </xsl:attribute>
            <xsl:value-of select="introduction/name"/>
        </fo:block>

        <fo:block padding-top="15pt">
            <xsl:apply-templates select="introduction/background"/>
        </fo:block>

        <fo:block font-size="15pt" font-weight="400" text-align="center" margin-top="30pt" id="{generate-id(pictures)}">
            Pictures
        </fo:block>

        <xsl:apply-templates select="pictures/picture"/>
        <xsl:apply-templates select="pictures/photos/photo"/>

        <xsl:apply-templates select="section"/>
    </xsl:template>

    <xsl:template match="introduction/background">
        <xsl:for-each select="paragraph">
            <fo:block margin-bottom="8pt" text-indent="15pt">
                <xsl:value-of select="."/>
            </fo:block>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="picture|photo">
        <fo:block text-align="center" padding-top="15pt">
            <fo:block>
                <fo:external-graphic height="200pt"
                                     content-width="scale-to-fit"
                                     scaling="uniform">
                    <xsl:attribute name="src">
                        url('<xsl:value-of select="."/>')
                    </xsl:attribute>
                </fo:external-graphic>
            </fo:block>
            <fo:block>
                <xsl:value-of select="@description"/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="section">
        <fo:block font-size="15pt" font-weight="400" text-align="center" margin-top="30pt" id="{generate-id(.)}">
            <xsl:value-of select="@name"/>
        </fo:block>

        <xsl:apply-templates select="detail|list-detail|nested-detail|histogram"/>
    </xsl:template>

    <xsl:template match="section/detail">
        <fo:block margin-top="5pt">
            <fo:inline font-weight="600">
                <xsl:value-of select="@name"/>
            </fo:inline>
            :
            <fo:inline margin-left="15pt">
                <xsl:value-of select="."/>
            </fo:inline>
        </fo:block>
    </xsl:template>

    <xsl:template match="list-detail">
        <fo:block margin-top="5pt">
            <fo:inline font-weight="600">
                <xsl:value-of select="@name"/>
            </fo:inline>
            :
            <fo:block margin-left="15pt">
                <fo:list-block provisional-distance-between-starts="0.3cm" provisional-label-separation="0.15cm">
                    <xsl:apply-templates/>
                </fo:list-block>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="list-item">
        <fo:list-item>
            <fo:list-item-label end-indent="label-end()">
                <fo:block>
                    <fo:inline>&#183;</fo:inline>
                </fo:block>
            </fo:list-item-label>
            <fo:list-item-body start-indent="body-start()">
                <fo:block>
                    <xsl:value-of select="."/>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>

    <xsl:template match="nested-detail">
        <fo:block margin-top="5pt">
            <fo:inline font-weight="600">
                <xsl:value-of select="@name"/>
            </fo:inline>
            :
            <fo:block margin-left="15pt">
                <fo:list-block provisional-distance-between-starts="0.3cm" provisional-label-separation="0.15cm">
                    <xsl:apply-templates select="detail|list-detail"/>
                </fo:list-block>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="nested-detail/detail">
        <fo:list-item>
            <fo:list-item-label end-indent="label-end()">
                <fo:block>
                    <fo:inline>&#183;</fo:inline>
                </fo:block>
            </fo:list-item-label>
            <fo:list-item-body start-indent="body-start()">
                <fo:block>
                    <fo:inline font-weight="600">
                        <xsl:value-of select="@name"/>
                    </fo:inline>
                    :
                    <fo:inline margin-left="5pt">
                        <xsl:value-of select="."/>
                    </fo:inline>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>

    <xsl:template match="nested-detail/list-detail">
        <fo:list-item>
            <fo:list-item-label end-indent="label-end()">
                <fo:block>
                    <fo:inline>&#183;</fo:inline>
                </fo:block>
            </fo:list-item-label>
            <fo:list-item-body start-indent="body-start()">
                <fo:block>
                    <fo:inline font-weight="600">
                        <xsl:value-of select="@name"/>
                    </fo:inline>
                    :
                    <fo:list-block provisional-distance-between-starts="0.3cm" provisional-label-separation="0.15cm">
                        <xsl:apply-templates select="list-item"/>
                    </fo:list-block>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>

    <xsl:template match="histogram">
        <fo:block margin-top="5pt">
            <fo:inline font-weight="600">
                <xsl:value-of select="@name"/>
            </fo:inline>
            :
            <xsl:if test="not(empty(@description))">
                <fo:inline font-size="7pt">
                    (<xsl:value-of select="@description"/>)
                </fo:inline>
            </xsl:if>
            <fo:table table-layout="fixed" width="100%">
                <fo:table-body>
                    <xsl:apply-templates select="entry"/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="entry">
        <fo:table-row>
            <fo:table-cell column-number="1">
                <fo:block>
                    <xsl:choose>
                        <xsl:when test="empty(@from-age) and empty(@to-age)">
                            <fo:inline font-weight="500">Overall</fo:inline>:
                        </xsl:when>
                        <xsl:when test="(@from-age = '0') and (@to-age = '0')">
                            <fo:inline font-weight="500">At birth</fo:inline>:
                        </xsl:when>
                        <xsl:when test="empty(@to-age)">
                            <fo:inline font-weight="500">
                                Over
                                <xsl:value-of select="@from-age"/>
                            </fo:inline>
                            :
                        </xsl:when>
                        <xsl:otherwise>
                            <fo:inline font-weight="500">
                                From age
                                <xsl:value-of select="@from-age"/>
                                to age
                                <xsl:value-of select="@to-age"/>
                            </fo:inline>
                            :
                        </xsl:otherwise>
                    </xsl:choose>
                </fo:block>
            </fo:table-cell>

            <fo:table-cell column-number="2">
                <fo:block>
                    <xsl:value-of select="."/>
                    <xsl:value-of select="../@unit"/>
                </fo:block>
            </fo:table-cell>

        </fo:table-row>
    </xsl:template>

</xsl:stylesheet>